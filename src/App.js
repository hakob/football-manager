import React, { Component } from 'react';
import './App.scss';
import Header from "./components/header/header";
import Main from './routes'

class App extends Component {
  render() {
    return (
        <div className="App">
          <Header />
          <Main/>
        </div>
    );
  }
}

export default App;
