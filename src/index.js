import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import { Provider } from 'react-redux';
import Store from "./store";
import {BrowserRouter as Router} from 'react-router-dom';
import {loadState, saveState} from "./localStorage";

// load state from localStorage if exists
const initialState = loadState();

const store = Store(initialState);
// subscribe to state changes
store.subscribe(() => {
    saveState({
        players: store.getState().players,
        teams: store.getState().teams,
        leagues: store.getState().leagues,
    });
});

console.log(store);
ReactDOM.render(
    <Provider store={store}>
        <Router>
            <App/>
        </Router> </Provider>, document.getElementById('root'));
