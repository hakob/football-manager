import React from 'react';
import {Switch, Route} from 'react-router-dom'
import Home from './components/home/home';
import PlayersList from "./components/players/playerList";
import  AddPlayer from "./components/players/addPlayer";
import Teamlist from "./components/teams/teamList";
import AddTeam from "./components/teams/addTeam";
import ViewTeam from "./components/teams/viewTeam";
import  LeagueList from "./components/leagues/leagueList";
import  AddLeague from "./components/leagues/addLeague";
import  ViewLeague from "./components/leagues/viewLeague"



const Main = () => (
    <main>
        <Switch>
            <Route exact path='/' component={Home}/>
            <Route path='/home' component={Home}/>
            <Route path='/playersList' component={PlayersList}/>
            <Route path='/addPlayer' component={AddPlayer}/>
            <Route path='/teamList' component={Teamlist}/>
            <Route path='/addTeam' component={AddTeam}/>
            <Route path='/viewTeam/:teamId' component={ViewTeam}/>
            <Route path='/leagueList' component={LeagueList}/>
            <Route path='/addLeague' component={AddLeague}/>
            <Route path='/viewLeague/:leagueId' component={ViewLeague}/>


        </Switch>
    </main>
);

export default Main
