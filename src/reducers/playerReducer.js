import * as actionTypes from '../actions/types';

export default (state = [], action) => {
    switch (action.type) {
        case actionTypes.CREATE_PLAYER:
            return [
                ...state,
                Object.assign({}, action.player)
            ];
        case actionTypes.MOVE_PLAYER:
            return state.map(item => {
                if (item.id === action.playerId) {
                    return {
                        ...item,
                        teamId: action.teamId
                    }
                }

                return item;
            });
        case actionTypes.REMOVE_PLAYER_FROM_TEAM:
            return state.map(item => {
                if (item.id === action.playerId && item.teamId === action.teamId) {
                    return {
                        ...item,
                        teamId: undefined
                    }
                }

                return item;
            });
        default:
            return state;
    }
};