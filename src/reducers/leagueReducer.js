import * as actionTypes from '../actions/types';

export default (state = [], action) => {
    switch (action.type){
        case actionTypes.CREATE_LEAGUE:
            return [
                ...state,
                Object.assign({}, action.league)
            ];

        case actionTypes.ADD_TEAM:
            return state.map(item => {
                if (item.id === action.leagueId) {
                    return {
                        ...item,
                        teams: Array.from(new Set(item.teams).add(action.teamId))
                    }
                }

                return item;
            });
        default:
            return state;
    }
};