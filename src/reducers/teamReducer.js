import * as actionTypes from '../actions/types';

export default (state = [], action) => {
    switch (action.type){
        case actionTypes.CREATE_TEAM:
            return [
                ...state,
                Object.assign({}, action.team)
            ];

        default:
            return state;
    }
};