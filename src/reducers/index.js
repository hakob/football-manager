import { combineReducers } from 'redux';
import players from './playerReducer';
import  teams from './teamReducer';
import leagues from './leagueReducer'

export default combineReducers({
    players: players,
    teams:teams,
    leagues:leagues
});