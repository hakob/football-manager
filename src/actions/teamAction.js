import * as types from './types';
import id from '../utils/id';

export const createTeam = (team) => {
    return {
        type: types.CREATE_TEAM,
        team: {...team, id:id()}
    }
};

// export const deleteContact = (id) => {
//     return {
//         type: types.DELETE_PlAYER,
//         id: id
//     }
// }