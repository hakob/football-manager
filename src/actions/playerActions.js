import * as types from './types';
import id from '../utils/id';

export const createPlayer = (player) => {
    return {
        type: types.CREATE_PLAYER,
        player: {...player,  id:id()}
    }
};

export const addPlayerToTeam = (playerId, teamId) => {
    return {
        type: types.MOVE_PLAYER,
        playerId,
        teamId
    }
};

export const removePlayerFromTeam = (playerId, teamId) => {
    return {
        type: types.REMOVE_PLAYER_FROM_TEAM,
        playerId,
        teamId
    }
};
