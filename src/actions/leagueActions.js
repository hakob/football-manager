import * as types from './types';
import id from '../utils/id';

export const createLeague = (league) => {
    if(!league.teams){
        league.teams = [];
    }

    return {
        type: types.CREATE_LEAGUE,
        league: {...league, id: id()},
    }
};

export const addTeam = (leagueId, teamId) => {
    return {
        type: types.ADD_TEAM,
        leagueId: leagueId,
        teamId: teamId
    }
};