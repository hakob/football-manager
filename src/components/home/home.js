import React, { Component } from 'react';
import './home.scss'
import {connect} from "react-redux";

class Home extends Component {
    render() {
        return(
            <div className="container center-div centered">
                <h1>Statistics</h1>
                <div className="info">
                    <span>Players Count</span>
                    <span>{this.props.players.length}</span>
                </div>
                <div className="info">
                    <span>Teams Count</span>
                    <span>{this.props.teams.length}</span>
                </div>
                <div className="info">
                    <span>Leagues Count</span>
                    <span>{this.props.leagues.length}</span>
                </div>
            </div>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
        teams: state.teams,
        players: state.players,
        leagues: state.leagues,
    }
};


export default connect(mapStateToProps)(Home);