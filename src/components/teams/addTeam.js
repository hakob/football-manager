import React from 'react';
import {Redirect} from 'react-router';
import {connect} from 'react-redux';
import * as teamAction from "../../actions/teamAction";
import 'react-confirm-alert/src/react-confirm-alert.css';

class Addteam extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: undefined,
            players: [],
        };

        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleSubmit(e) {
        e.preventDefault();
        let team = {
            name: this.state.name
        };

        let isValid = true;
        if (team.name === undefined) {
            isValid = false;
            this.setState({hasError: true})
        } else {
            this.setState({hasError: false})
        }


        if (isValid) {
            this.setState({fireRedirect: true});
            this.props.createTeam(team);
        }
    }

    handleChange(e) {
        this.setState({[e.target.name]: e.target.value})
    }

    render() {
        return (
            <div className="container center-div" id="responsive">
                <form className='form contact-form'>
                        <div className="container ">
                            <div className="form-group">
                                <input type="text" id="disabledTextInput" name='name' className="form-control"
                                       placeholder="Team Name"
                                       value={this.state.name}
                                       onChange={this.handleChange.bind(this)}/>
                                {this.state.hasError === true && (
                                    <span className="text-danger">Required</span>
                                )}
                            </div>
                        </div>
                        <div className="form-group">
                            <span className="btn btn-primary inline-block margin10"
                                  onClick={this.handleSubmit}>Submit
                            </span>
                        </div>
                        {this.state.fireRedirect && (
                            <Redirect to={'/teamList'}/>
                        )}
                </form>

            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createTeam: team => dispatch(teamAction.createTeam(team)),
    }
};

const mapStateToProps = (state) => {
    return {
        players: state.players
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Addteam);