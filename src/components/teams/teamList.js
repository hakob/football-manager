import React, {Component} from 'react';
import {connect} from 'react-redux';
import Modal from 'simple-react-modal';
import Select from 'react-select';
import * as playerAction from "../../actions/playerActions";

class TeamList extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    };


    handleClick = () => {
        this.props.history.push('/addTeam')
    };

    handleView = (id) => {
        this.props.history.push({
            pathname: `/viewTeam/${id}`
        })
    };

    handleAddPlayer = (id) => {
        this.setState({
            showAddPlayer: true,
            selectedTeam: id
        });
    };

    closeAddPlayer() {
        this.setState({showAddPlayer: false})
    };

    handleAddPlayerChange = (selectedPlayersToAdd) => {
        this.setState({selectedPlayersToAdd});
    };

    submitAddPlayer = () => {
        let players = this.state.selectedPlayersToAdd;
        for (let p of players){
            this.props.addPlayerToTeam(p.value, this.state.selectedTeam);
        }

        this.setState({selectedPlayersToAdd: undefined});

        this.closeAddPlayer();
    };


    handleMovePlayer = (id) => {
        this.setState({
            showMovePlayer: true,
            selectedTeam: id
        });
    };

    closeMovePlayer() {
        this.setState({showMovePlayer: false})
    }

    handleMovePlayerChange = (selectedPlayersToMove) => {
        this.setState({selectedPlayersToMove});
    };

    submitMovePlayer = () => {
        let players = this.state.selectedPlayersToMove;
        for (let p of players){
            this.props.addPlayerToTeam(p.value, this.state.selectedTeam);
        }

        this.setState({selectedPlayersToMove: undefined});

        this.closeMovePlayer();
    };

    handleRemovePlayer = (id) => {
        this.setState({
            showRemovePlayer: true,
            selectedTeam: id
        });
    };

    closeRemovePlayer() {
        this.setState({showRemovePlayer: false})
    }

    handleRemovePlayerChange = (selectedPlayerToRemove) => {
        this.setState({selectedPlayerToRemove});
    };

    submitRemovePlayer = () => {
        let player = this.state.selectedPlayerToRemove;
        this.props.removePlayerFromTeam(player.value, this.state.selectedTeam);

        this.setState({selectedPlayerToRemove: undefined});

        this.closeRemovePlayer();
    };


    getTeamPlayers(id) {
        return this.props.players.filter(p => p.teamId === id).map(p => {
            return {
                value: p.id,
                label: p.name
            }
        })
    }

    getFreePlayers(id) {
        return this.props.players.filter(p => !p.teamId).map(p => {
            return {
                value: p.id,
                label: p.name
            }
        })
    }

    getMovePlayers(id) {
        let teamMap = {};
        for(let t of this.props.teams){
            teamMap[t.id] = t;
        }

        return this.props.players.filter(p => p.teamId && p.teamId !== id).map(p => {
            return {
                value: p.id,
                label: p.name + " | team: " + teamMap[p.teamId].name
            }
        })
    }

    render() {
        return (
            <div className="center-div">
                <div className="add-row">
                    <span className="btn btn-primary" onClick={() => {
                        this.handleClick()
                    }}>+Add Team</span>
                </div>
                <table id="teams-table" className="table table-striped">
                    <thead>
                    <th> Team Name</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    </thead>
                    <tbody>
                    {(this.props.teams.map((d) => {
                        return (
                            <tr>
                                <td>{d.name}</td>
                                <td><span className="btn btn-primary divider"
                                       onClick={() => this.handleView(d.id)}>View Team</span></td>
                                <td><span className="btn btn-primary divider"
                                       onClick={() => this.handleAddPlayer(d.id)}>Add Player</span></td>
                                <td><span className="btn btn-primary divider"
                                       onClick={() => this.handleMovePlayer(d.id)}>Move Player</span></td>
                                <td><span className="btn btn-primary divider"
                                       onClick={() => this.handleRemovePlayer(d.id)}>Remove Player</span></td>
                            </tr>
                        )
                    }))}
                    </tbody>
                </table>
                <Modal show={this.state.showAddPlayer} onClose={this.closeAddPlayer.bind(this)}>
                    <h1>Add Player to team</h1>
                    <Select
                        isMulti
                        name="colors"
                        value={this.state.selectedPlayersToAdd}
                        onChange={this.handleAddPlayerChange}
                        options={this.getFreePlayers(this.state.selectedTeam)}
                        className="basic-multi-select"
                        classNamePrefix="select"
                    />
                    <div>
                        <span className="btn btn-primary inline-block margin10" onClick={this.submitAddPlayer}>Add
                            Players</span>
                    </div>
                </Modal>
                <Modal show={this.state.showMovePlayer} onClose={this.closeMovePlayer.bind(this)}>
                    <h1>Move player</h1>
                    <Select
                        isMulti
                        name="colors"
                        value={this.state.selectedPlayersToMove}
                        options={this.getMovePlayers(this.state.selectedTeam)}
                        className="basic-multi-select"
                        onChange={this.handleMovePlayerChange}
                        classNamePrefix="select"
                    />
                    <div>
                        <span className="btn btn-primary inline-block margin10" onClick={this.submitMovePlayer}>Move
                            Player</span>
                    </div>
                </Modal>
                <Modal show={this.state.showRemovePlayer} onClose={this.closeRemovePlayer.bind(this)}>
                    <h1>Remove player</h1>
                    <Select
                        name="colors"
                        options={this.getTeamPlayers(this.state.selectedTeam)}
                        value={this.state.selectedPlayerToRemove}
                        onChange={this.handleRemovePlayerChange}
                        className="basic-multi-select"
                        classNamePrefix="select"
                    />
                    <div>
                        <span className="btn btn-primary inline-block margin10" onClick={this.submitRemovePlayer}>Remove
                            Player</span>
                    </div>
                </Modal>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        teams: state.teams,
        players: state.players,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        addPlayerToTeam: (player, teamId) => dispatch(playerAction.addPlayerToTeam(player, teamId)),
        removePlayerFromTeam: (player, teamId) => dispatch(playerAction.removePlayerFromTeam(player, teamId)),
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(TeamList);