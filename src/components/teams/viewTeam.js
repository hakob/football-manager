import React from 'react';
import {connect} from 'react-redux';
import * as teamAction from "../../actions/teamAction";
import "./teams.scss"

class ViewTeam extends React.Component {
    constructor(props) {
        super(props);

        let id = this.props.match.params.teamId;
        let teamPlayers = this.props.players.filter(p => p.teamId === id);
        let team = this.props.teams.find(p => p.id === id);

        this.state = {
            id: this.props.id,
            name: team.name,
            players: teamPlayers,
        };
    }

    render() {
        return (
            <div className="container center-div" id="responsive">
                <h1 className="centered">{this.state.name}</h1>
                <h2>Players list</h2>
                <ol className="players-list">{this.state.players.map(p => <li>{p.name}</li>)}</ol>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createTeam: team => dispatch(teamAction.createTeam(team)),
    }
};

const mapStateToProps = (state) => {
    return {
        teams: state.teams,
        players: state.players
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(ViewTeam);