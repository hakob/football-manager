import React from 'react';
import {NavLink} from 'react-router-dom';
import './header.scss';


class Header extends React.Component {

    render() {
        return (
            <div id="navigation-bar">
                <nav>
                    <ul className="navbar-nav">
                        <li className="nav-item ">
                            <NavLink to="/home" className={"nav-link"}>
                                <span className='navText'>Home</span>
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/playersList" className={"nav-link"}>
                                <span className='navText'>Players</span>
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/teamlist" className={"nav-link"}>
                                <span className='navText'>Teams</span>
                            </NavLink>
                        </li>

                        <li className="nav-item">
                            <NavLink to="/leagueList" className={"nav-link"}>
                                <span className='navText'>Leagues</span>
                            </NavLink>
                        </li>
                    </ul>
                </nav>
            </div>
        )
    }
}

export default Header;
