import React, {Component} from 'react';
import {connect} from 'react-redux';
import Modal from "simple-react-modal";
import Select from 'react-select';
import * as leagueActions from "../../actions/leagueActions";

class LeagueList extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    };

    handleClick = () => {
        this.props.history.push('/addLeague')

    };

    handleView = (id) => {
        this.props.history.push({
            pathname: `/viewLeague/${id}`
        })
    };

    handleAddTeams = (id) => {
        this.setState({
            showAddTeams: true,
            selectedLeague: id
        });
    };

    closeAddTeams() {
        this.setState({showAddTeams: false})
    };

    handleAddTeamsChange = (selectedTeamsToAdd) => {
        this.setState({selectedTeamsToAdd});
    };

    submitAddTeams = () => {
        let players = this.state.selectedTeamsToAdd;
        for (let p of players){
            this.props.addTeam(this.state.selectedLeague, p.value);
        }

        this.setState({selectedTeamsToAdd: undefined});

        this.closeAddTeams();
    };

    getTeams(lid){
        let league = this.props.leagues.find(l=>l.id===lid);

        return this.props.teams.filter(t => !league.teams.includes(t.id)).map(t => {
            return {
                value: t.id,
                label: t.name
            }
        })
    }


    render() {
        return (
            <div className="center-div">
                <div className="add-row">
                    <span className="btn btn-primary" onClick={() => {
                        this.handleClick()
                    }}>+Add Team</span>
                </div>
                <table id="leagues-table" className="table table-striped">
                    <thead>
                    <th> League Name</th>
                    <th></th>
                    <th></th>
                    </thead>
                    <tbody>
                    {this.props.leagues.map(l => {
                        return (
                            <tr>
                                <td>{l.name}</td>
                                <td><span className="btn btn-primary divider"
                                          onClick={() => this.handleView(l.id)}>View League</span></td>
                                <td><span className="btn btn-primary divider"
                                          onClick={() => this.handleAddTeams(l.id)}>Add Team</span></td>
                            </tr>)
                    })}
                    </tbody>
                </table>
                <Modal show={this.state.showAddTeams} onClose={this.closeAddTeams.bind(this)}>
                    <h1>Add Player to team</h1>
                    <Select
                        isMulti
                        name="colors"
                        value={this.state.selectedTeamsToAdd}
                        onChange={this.handleAddTeamsChange}
                        options={this.state.selectedLeague && this.getTeams(this.state.selectedLeague)}
                        className="basic-multi-select"
                        classNamePrefix="select"
                    />
                    <div>
                        <span className="btn btn-primary inline-block margin10" onClick={this.submitAddTeams}>Add
                            Leagues</span>
                    </div>
                </Modal>
            </div>
        );

    }
}

const mapStateToProps = state => {
    return {
        leagues: state.leagues,
        teams: state.teams
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        addTeam: (player, teamId) => dispatch(leagueActions.addTeam(player, teamId)),
    }
};


export default connect(mapStateToProps,mapDispatchToProps)(LeagueList);