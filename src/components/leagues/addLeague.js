import React from 'react';
import {Redirect} from 'react-router';
import {connect} from 'react-redux';
import * as leagueAction from "../../actions/leagueActions";
import 'react-confirm-alert/src/react-confirm-alert.css';


class Addleague extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: undefined,
            teams: [],
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        this.setState({[e.target.name]: e.target.value})
    }

    handleSubmit(e) {
        e.preventDefault();
        let league = {
            name: this.state.name
        };
        if (league.name === undefined) {
            this.setState({hasError: true})
        } else {
            this.setState({fireRedirect: true});
            this.props.createLeague(league);
        }
    }

    render() {
        return (
            <div className="container center-div" id="responsive">
                <form className='form contact-form'>
                    <div className="container ">
                        <div className="form-group">
                            <input type="text" id="disabledTextInput" name='name' className="form-control"
                                   placeholder="League Name"
                                   value={this.state.name}
                                   onChange={this.handleChange.bind(this)}/>
                            {this.state.hasError === true && (
                                <span className="text-danger">Required</span>
                            )}
                        </div>
                    </div>
                    <div className="form-group">
                            <span className="btn btn-primary inline-block margin10"
                                  onClick={this.handleSubmit}>Submit
                            </span>
                    </div>
                    {this.state.fireRedirect && (
                        <Redirect to={'/leagueList'}/>
                    )}
                </form>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createLeague: league => dispatch(leagueAction.createLeague(league)),

    }
};

const mapStateToProps = (state, ownProps) => {
    return {
        teams: state.teams
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Addleague);