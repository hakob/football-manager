import React from 'react';
import {connect} from 'react-redux';
import "./leagues.scss"

class ViewLeague extends React.Component {
    constructor(props) {
        super(props);

        let id = this.props.match.params.leagueId;
        let league = this.props.leagues.find(p => p.id === id);

        this.state = {
            id: this.props.id,
            name: league.name,
            teams: league.teams.map(t=>this.props.teams.find(team=>team.id===t)),
        };
    }

    render() {
        return (
            <div className="container center-div" id="responsive">
                <h1 className="centered">{this.state.name}</h1>
                <h2>Teams in league</h2>
                <ol className="teams-list">{this.state.teams.map(p => <li>{p.name}</li>)}</ol>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        teams: state.teams,
        leagues: state.leagues
    }
};
export default connect(mapStateToProps)(ViewLeague);