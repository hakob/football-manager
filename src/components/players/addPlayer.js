import React from 'react';
import {Redirect} from 'react-router';
import {connect} from 'react-redux';
import * as playerAction from "../../actions/playerActions";
import 'react-confirm-alert/src/react-confirm-alert.css'
import './players.scss'

class AddPlayer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: undefined,
            position: undefined,
            age: undefined,
            height: undefined,
            weight: undefined
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();
        let player = {
            name: this.state.name,
            position: this.state.position,
            age: this.state.age,
            height: this.state.height,
            weight: this.state.weight

        };

        let isValid = true;
        const fields = ["name", "position", "age", "height", "weight"];

        for(let f of fields){
            if(player[f] === undefined){
                isValid = false;
                this.setState({[f + "Error"]: true});
            } else {
                this.setState({[f + "Error"]: false});
            }
        }

        if (isValid) {
            this.setState({
                name: undefined,
                position: undefined,
                age: undefined,
                height: undefined,
                weight: undefined

            });
            this.setState({fireRedirect: true});
            this.props.createPlayer(player);
        }
    }


    handleChange(e) {
        this.setState({[e.target.name]: e.target.value})
    }

    render() {
        return (
            <div className="container center-div" id="responsive">
                <form className='form contact-form'>
                    <div className="container ">
                        <div className="form-group">
                            <input type="text" id="disabledTextInput" name='name' className="form-control"
                                   placeholder="Name"
                                   value={this.state.name}
                                   onChange={this.handleChange.bind(this)}/>
                            {this.state.nameError === true && (
                                <span className="text-danger">Required</span>
                            )}
                        </div>
                        <div className="form-group">
                            <input type="text" className="form-control"
                                   name='position'
                                   placeholder="Position"
                                   value={this.state.position}
                                   onChange={this.handleChange.bind(this)}/>
                            {this.state.positionError === true && (
                                <span className="text-danger">Required</span>
                            )}
                        </div>
                        <div className="form-group">
                            <input type="number" id="disabledTextInput" className="form-control"
                                   name='age'
                                   placeholder="Age"
                                   value={this.state.age}
                                   onChange={this.handleChange.bind(this)}/>
                            {this.state.ageError === true && (
                                <span className="text-danger">Required</span>
                            )}
                        </div>
                        <div className="form-group">
                            <input type="number" id="disabledTextInput" className="form-control"
                                   name='height'
                                   placeholder="Height"
                                   value={this.state.height}
                                   onChange={this.handleChange.bind(this)}/>
                            {this.state.heightError === true && (
                                <span className="text-danger">Required</span>
                            )}
                        </div>
                        <div className="form-group">
                            <input type="number" id="disabledTextInput" className="form-control"
                                   name='weight'
                                   placeholder="Weight"
                                   value={this.state.weight}
                                   onChange={this.handleChange.bind(this)}/>
                            {this.state.weightError === true && (
                                <span className="text-danger">Required</span>
                            )}
                        </div>
                        <div className="form-group">
                            <span className="btn btn-primary inline-block margin10"
                                  onClick={this.handleSubmit}>Submit
                            </span>
                        </div>
                    </div>
                    {this.state.fireRedirect && (
                        <Redirect to={'/playersList'}/>
                    )}
                </form>

            </div>

        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createPlayer: player => dispatch(playerAction.createPlayer(player)),

    }
};

export default connect(null, mapDispatchToProps)(AddPlayer);