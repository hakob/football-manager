import React, {Component} from 'react';
import {connect} from 'react-redux';
import "./players.scss"


class PlayersList extends Component {
    handleClick = () => {
        this.props.history.push('/addPlayer')
    };

    render() {
        return (
            <div className='container center-div'>
                <div className="add-row">
                    <span className="btn btn-primary" onClick={() => {
                        this.handleClick()
                    }}>+Add Player</span>
                </div>
                <table className="player-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Age</th>
                        <th>Height</th>
                        <th>Weight</th>
                        <th>Current team</th>
                    </tr>
                    </thead>
                    <tbody>
                    {(this.props.players.map(d=> {
                        return (
                            <tr>
                                <td>{d.name}</td>
                                <td>{d.position}</td>
                                <td>{d.age}</td>
                                <td>{d.height} cm</td>
                                <td>{d.weight} kg</td>
                                <td>{(this.props.teams.find(t=>t.id===d.teamId) || {}).name || "Free"}</td>
                            </tr>
                        )
                    }))
                    }
                    </tbody>
                </table>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        players: state.players,
        teams: state.teams
    }
};


export default connect(mapStateToProps,)(PlayersList);